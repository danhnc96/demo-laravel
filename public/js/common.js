// alert error
function alertError(error){
    if(error?.status == 422){
        $('#success').addClass('d-none');
        $('#error').removeClass('d-none');
        let listError = error.responseJSON;
        var err = "<strong>ERROR!</strong>";
        let i = 1;
        for (const [key, value] of Object.entries(listError)) {
            err = `${err} </br>${i}. ${value}`;
            i++;
        }
        $('#errText').html(err);
        $('html, body').animate({
            scrollTop: $(".container").offset().top
        });
    }else{
        console.log(error);
    }
}

// alert success
function alertSuccess(mess){
    $('#error').addClass('d-none');
    $('#success').removeClass('d-none');
    $('#succText').html(`<strong>SUCCESS!</strong></br>${mess}`);
    $('html, body').animate({
        scrollTop: $(".container").offset().top
    });
}

$('.load-image').change(function() {
    let reader = new FileReader();
    if(this.files[0]){
        reader.onload = (e) => {
            $('#preview-image-before-upload').removeClass('d-none');
            $('#preview-image-before-upload').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
    }
    if($('.load-image').val() == ''){
        $('#preview-image-before-upload').addClass('d-none');
    }
});

// update product detail
function updateProductDetail(id, product_id, total_inv, stocked_inv, available_inv, unit_price, bulk_price, url){
    $.ajax({
        url: url,
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            product_id: product_id,
            id: id,
            total_inv: total_inv,
            stocked_inv: stocked_inv,
            available_inv: available_inv,
            unit_price: unit_price,
            bulk_price: bulk_price,
        },
        success: function(result) {
            alert("Create new product successful!");
            location.reload();
        },
        error: function(error) {
            if(error?.status == 422){
                let listError = error.responseJSON;
                var err = "";
                for (const [key, value] of Object.entries(listError)) {
                    err = `${err} \r ${value}`;
                }
                alert(err);
            }else{
                alert(err);
                console.log(error);
            }
        }
   });
}