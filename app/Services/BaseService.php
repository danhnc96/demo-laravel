<?php
namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class BaseService {

    const LENGTH_CODE = 5;
    const PAGINATE = 10;

    /**
     * Auth User
     * @return json user
     */
    public function getAuthUser()
    {
        return [
            'id' => Auth::id(),
            'name' => Auth::user()->name,
            'email' => Auth::user()->email,
        ];
    }

     /**
     * Upload files to storage
     *
     * @param $files
     * @return path files
     */
    public function uploadFile($files)
    {
        try {
            $imagePath = $files;
            $imageName = $imagePath->getClientOriginalName();
            $filename = explode('.', $imageName)[0];
            $extension = $imagePath->getClientOriginalExtension();
            $picName =  Str::slug(time()."_".$filename, "_").".". $extension;
            
            $path = $files->storeAs('uploads', $picName, 'public');

            return $path;
        } catch (\Throwable $th) {
            Log::error($th);
            throw $th;
        }
    }
}
