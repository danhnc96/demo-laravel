<?php
namespace App\Services;


use App\Models\Transaction;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TransactionService extends BaseService
{
    protected $TransactionService;
    public function __construct(
        Transaction $Transaction
    )
    {
        $this->Transaction = $Transaction;
    }

    public function index()
    {
        try {
            $data =Transaction::with('Product')->with('CustomerInformation')->orderBy('created_at', 'DESC')->paginate(self::PAGINATE);
            return $data;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Show transaction detail
     * @param $request input form data
     * @return list transaction
     */
    public function show($id)
    {
        try {
            $data =Transaction::with('Product')->with('CustomerInformation')->find($id);
            if(!$data){ abort(404); }
            return $data;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Update Transaction
     * @param $request input form data
     * @return inserted contacts id
     */
    public function update($request)
    {
        Log::debug($request);
        try {
            DB::beginTransaction();
            $transaction = Transaction::where('id', $request->id)->update($request->all());
            DB::commit();
            return $transaction;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}