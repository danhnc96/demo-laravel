<?php
namespace App\Services;

use App\Models\CustomerInformation;
use App\Models\Transaction;
use Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CustomerInformationService extends BaseService
{
    protected $CustomerInformation;
    public function __construct(
        CustomerInformation $CustomerInformation,
        Transaction $Transaction
    )
    {
        $this->CustomerInformation = $CustomerInformation;
        $this->Transaction = $Transaction;
    }

    public function index()
    {
        
    }

    /**
     * Create Customer Information
     * @param $request input form data
     * @return inserted contacts id
     */
    public function store($request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $customer = CustomerInformation::create($data);
            $quantity = $request->quantity;
            $unit_price = $request->unit_price;
            $bulk_price = $request->bulk_price;
            $totalMoney = 0;
            if($request->quantity < 50){
                $totalMoney = $unit_price * $quantity;
            }else{
                $totalMoney = $bulk_price * $quantity;
            }
            $transaction = Transaction::create([
                'product_id' => $request->product_id,
                'customer_information_id' => $customer->id,
                'quantity' => $request->quantity,
                'unit_price' => $request->unit_price,
                'bulk_price' => $request->bulk_price,
                'delivery' => $request->delivery,
                'total_money' => $totalMoney,
                'delivery_at' => Carbon::now(),
                'delivery_address' => $request->address,
            ]);
            return $transaction->id;
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Show Customer Information deatail
     * @param $request input form data
     * @return inserted contacts id
     */
    public function show($id)
    {
        try {
            $data = CustomerInformation::find($id);
            if(!$data){ abort(404); }
            return $data;
        } catch (Exception $e) {
            $e->errorInfo = "error";
            throw $e;
        }
    }

    /**
     * Update Customer Information
     * @param $request input form data
     * @return inserted contacts id
     */
    public function update($request)
    {
        try {
            DB::beginTransaction();
            CustomerInformation::where('id', $request->customer_id)->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'country_code' => $request->country_code,
                'phone' => $request->phone,
                'email' => $request->email,
                'address' => $request->address,
                'suburb' => $request->suburb,
                'state' => $request->state,
                'postcode' => $request->postcode,
                'country' => $request->country,
            ]);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}