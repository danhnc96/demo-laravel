<?php
namespace App\Services;

use App\Models\Contact;
use Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class ContactService extends BaseService
{
    protected $Contacts;
    public function __construct(
        Contact $Contacts
    )
    {
        $this->Contacts = $Contacts;
    }

    public function index()
    {
        try {
            $data = $this->Contacts->whereNull('deleted_at')->orderBy('created_at', 'DESC')->paginate(self::PAGINATE);
            return $data;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Create contacts
     * @param $request input form data
     * @return inserted contacts id
     */
    public function store($request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            if(!empty($request->file('photo'))){
                $path = $this->uploadFile($request->file('photo'));
                $data['photo'] = $path;
            }
            $data['user_create'] = json_encode($this->getAuthUser());
            Contact::create($data);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Show contacts deatail
     * @param $request input form data
     * @return inserted contacts id
     */
    public function show($id)
    {
        try {
            $data = Contact::find($id);
            if(!$data){ abort(404); }
            $data->photo = Storage::url($data->photo);
            return $data;
        } catch (Exception $e) {
            $e->errorInfo = "error";
            throw $e;
        }
    }

    /**
     * Update contacts
     * @param $request input form data
     * @return inserted contacts id
     */
    public function update($request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            if(!empty($request->file('photo'))){
                $path = $this->uploadFile($request->file('photo'));
                $data['photo'] = $path;
            }
            $data['user_update'] = $this->getAuthUser();
            unset($data['_token']);
            $contact = Contact::where('id', $request->id)->update($data);
            DB::commit();
            return $contact;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * deleted contact
     * @param $id contact
     * @return $mess
     */
    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $data['deleted_at'] = now();
            $data['user_delete'] = $this->getAuthUser();
            Contact::where('id', $id)->update($data);
            DB::commit();
            return $id;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}