<?php
namespace App\Services;

use App\Models\Product;
use App\Models\ProductDetail;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ProductService extends BaseService
{
    protected $Product;
    protected $productDetail;
    public function __construct(
        Product $Product,
        ProductDetail $ProductDetail
    )
    {
        $this->Product = $Product;
        $this->ProductDetail = $ProductDetail;
    }

    public function index()
    {
        try {
            $data = Product::with('ProductDetail')->paginate(self::PAGINATE);
            return $data;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function create()
    {
        try{
            $code = Str::random(self::LENGTH_CODE);
            while (Product::where('code', $code)->first()){
                $code = Str::random(self::LENGTH_CODE);
            }
            return $code;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Create product
     * @param $request input form data
     * @return inserted contacts id
     */
    public function store($request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            if(!empty($request->images)){
                $path = [];
                foreach ($request->images as $key => $image) {
                    $path[$key] = $this->uploadFile($image);
                }
                $data['image'] = json_encode($path);
            }
            $data['user_create'] = json_encode($this->getAuthUser());
            $data['slug'] = Str::slug($request->name, '-');
            Log::debug($data);
            $product = Product::create($data);
            $productDetail = ProductDetail::create([
                'product_id' => $product->id,
                'user_create' => $data['user_create']
            ]);
            DB::commit();
            return $productDetail;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Show product deatail
     * @param $request input form data
     * @return list product
     */
    public function show($id)
    {
        try {
            $data = Product::with('ProductDetail')->find($id);
            if(!$data){ abort(404); }
            return $data;
        } catch (Exception $e) {
            $e->errorInfo = "error";
            throw $e;
        }
    }

    /**
     * Show related product deatail
     * @param $related
     * @return list product
     */
    public function related($related, $id){
        try {
            return Product::with('ProductDetail')->where('related', $related)->where('id', '!=', $id)->first();
        } catch (Exception $e) {
            $e->errorInfo = "error";
            throw $e;
        }
    }

    /**
     * Update product
     * @param $request input form data
     * @return inserted product id
     */
    public function update($request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            if(!empty($request->images)){
                $path = [];
                foreach ($request->images as $key => $image) {
                    $path[$key] = $this->uploadFile($image);
                }
                $data['image'] = json_encode($path);
            }
            $data['user_update'] = $this->getAuthUser();
            $data['slug'] = Str::slug($request->name, '-');
            unset($data['_token']);
            unset($data['images']);
            $product = Product::where('id', $request->id)->update($data);
            DB::commit();
            return $product;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * update product detail
     * @param $request product deatail
     * @return $productDetail
     */
    public function updateDetail($request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $data['user_update'] = $this->getAuthUser();
            ProductDetail::where('id', $request->id)
            ->where('product_id', $request->product_id)
            ->update($data);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}