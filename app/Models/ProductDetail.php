<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductDetail extends Model
{
    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_detail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'total_inv',
        'stocked_inv',
        'available_inv',
        'unit_price',
        'bulk_price',
        'user_create',
        'user_update',
        'user_delete'
    ];

    // public function product()
    // {
    //     return $this->hasOne(Product::class, 'product_id');
    //     // return $this->belongsTo(Product::class, 'product_id');
    // }

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
}
