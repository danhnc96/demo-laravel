<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'product_id' => 'required',
            'total_inv' => 'required',
            'stocked_inv' => 'required',
            'available_inv' => 'required',
            'unit_price' => 'required',
            'bulk_price' => 'required',
        ];
    }
}
