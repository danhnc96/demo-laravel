<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductDetailRequest;
use App\Http\Controllers\Controller;
use App\Services\ProductService;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listProducts = $this->productService->index();
        return view('product.index', compact('listProducts'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $code =  $this->productService->create();
        return view('product.create', compact('code'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $productDetail = $this->productService->store($request);
        return response()->json(['success' => 'Post created successfully.', 'product_detail' => $productDetail]);
    }

    /**
     * Show contact detail.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $data = $this->productService->show($id);
        return view('product.edit', compact('data'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request)
    {
        $this->productService->update($request);
        return response()->json(['success' => 'Post created successfully.']);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ProductDetailRequest $request
     * @return \Illuminate\Http\Response
     */
    public function updateDetail(Request $request)
    {
        $this->productService->updateDetail($request);
        return response()->json(['success' => 'Post created successfully.']);
    }
}
