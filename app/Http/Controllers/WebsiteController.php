<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Services\CustomerInformationService;
use App\Http\Requests\CustomerInformationRequest;
use App\Http\Requests\CustomerUpdateRequest;
use App\Services\TransactionService;

class WebsiteController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $productService;
    protected $CustomerInformationService;
    protected $transactionService;

    public function __construct(ProductService $productService, CustomerInformationService $customerInformationService, TransactionService $transactionService)
    {
        $this->productService = $productService;
        $this->customerInformationService = $customerInformationService;
        $this->transactionService = $transactionService;
    }

    /**
     * Show product.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $listProducts = $this->productService->index();
        return view('website.index', compact('listProducts'));
    }

    /**
     * Show product.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function detail($id)
    {
        $product = $this->productService->show($id);
        $related = $this->productService->related($product->related, $product->id);
        return view('website.detail', compact('product', 'related'));
    }

    /**
     * Show invoice.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function invoice($id)
    {
        $data = $this->transactionService->show($id);
        return view('website.invoice', compact('data'));
    }

    /**
     * Create Customer Information
     * 
     * @param  \App\Http\Requests\CustomerInformationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function createCustomerInformation(CustomerInformationRequest $request)
    {
        $id = $this->customerInformationService->store($request);
        return response()->json(['success' => 'Post created successfully.', 'id' => $id]);
    }

    /**
     * Update Customer Information
     * 
     * @param  \App\Http\Requests\Request $request
     * @return \Illuminate\Http\Response
     */
    public function updateCustomerInformation(CustomerUpdateRequest $request)
    {
        $this->customerInformationService->update($request);
        return response()->json(['success' => 'Post created successfully.']);
    }
    
}
