<?php

namespace App\Http\Controllers;

use App\Services\TransactionService;
use Barryvdh\DomPDF\Facade\Pdf;

class DownloadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $transactionService;

    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    /**
     * Dowload Sample Invoice to Customer PDF
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function downloadPDF($id)
    {
        $data = $this->transactionService->show($id);
        $pdf = Pdf::loadView('Invoicepdf', compact('data'));
        $name = 'invoice_'.$data->id.'.pdf';
        return $pdf->download($name);
    }
}
