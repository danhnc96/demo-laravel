<?php

namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Services\TransactionService;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Barryvdh\DomPDF\Facade\Pdf;
use Exception;
use Illuminate\Support\Facades\Log;

class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $transactionService;

    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function sendMailInvoice(Request $request)
    {
        try{
            $data = $this->transactionService->show($request->id);
            $mailData = [
                'name' => $request->name,
                'pdf' =>  Pdf::loadView('Invoicepdf', compact('data')),
            ];
            Mail::to($request->email)->send(new SendMail($mailData));
            
            return response()->json(['success' => 'Send Mail successfully.']);
        }catch (Exception $e) {
            throw $e;
        }
    }
}
