<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\ProductService;
use App\Services\CustomerInformationService;
use App\Services\TransactionService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $productService;
    protected $CustomerInformationService;
    protected $transactionService;

    public function __construct(ProductService $productService, CustomerInformationService $customerInformationService, TransactionService $transactionService)
    {
        $this->productService = $productService;
        $this->customerInformationService = $customerInformationService;
        $this->transactionService = $transactionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listOrder = $this->transactionService->index();
        return view('order.index', compact('listOrder'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $data = $this->transactionService->show($id);
        return view('order.detail', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $this->transactionService->update($request);
        return response()->json(['success' => 'Post update successfully.']);
    }
}
