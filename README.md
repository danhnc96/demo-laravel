# LARAVEL DEMO
1. Laravel 9x (PHP v8.1.0)
    - Documentation: https://laravel.com/docs/9.x
2. Bootstrap v5.1.3
    - Documentation: https://getbootstrap.com/docs/5.1/getting-started/introduction/
## SETUP PROJECT
### Install Composer Dependencies
Enter the command: composer install

### Create your copy of the .env file
Enter the command: cp .env.example .env

### Generate an App Encryption Key
Enter the command: php artisan key:generate

### Migrate the database
Enter the command: php artisan migrate