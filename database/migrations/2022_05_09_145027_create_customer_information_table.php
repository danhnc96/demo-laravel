<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_information', function (Blueprint $table) {
            $table->id();
            $table->integer('title');
            $table->string('first_name')->index();
            $table->string('last_name')->index();
            $table->string('second_name')->index();
            $table->integer('country_code');
            $table->string('phone');
            $table->string('email')->index();
            $table->string('address');
            $table->string('suburb')->nullable();;
            $table->string('state')->nullable();;
            $table->string('postcode')->nullable();;
            $table->string('country');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_information');
    }
};
