<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products');
            $table->integer('customer_information_id')->unsigned();
            $table->foreign('customer_information_id')->references('id')->on('products');
            $table->integer('quantity');
            $table->decimal('unit_price', 15,2)->default(0.00);
            $table->decimal('bulk_price', 15,2)->default(0.00);
            $table->decimal('total_money', 15,2)->default(0.00);
            $table->integer('delivery')->default(1);
            $table->dateTime('delivery_at');
            $table->string('delivery_address');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
