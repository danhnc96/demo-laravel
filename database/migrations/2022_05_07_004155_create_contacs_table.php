<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->integer('title');
            $table->string('first_name')->index();
            $table->string('last_name')->index();
            $table->string('second_name')->index();
            $table->string('gender');
            $table->date('birthday');
            $table->string('photo')->nullable();
            $table->string('company');
            $table->integer('country_code');
            $table->string('phone')->index();
            $table->string('email')->index();
            $table->string('email_type');
            $table->string('address');
            $table->string('suburb');
            $table->string('state');
            $table->string('postcode');
            $table->string('country');
            $table->json('user_create')->nullable();
            $table->json('user_update')->nullable();
            $table->json('user_delete')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
};
