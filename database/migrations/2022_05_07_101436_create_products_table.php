<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        // product
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('code', 10)->unique();
            $table->string('name', 255);
            $table->string('slug', 255);
            $table->integer('related')->nullable();
            $table->json('image')->nullable();
            $table->mediumText('summary')->nullable();
            $table->mediumText('details')->nullable();
            $table->json('user_create')->nullable();
            $table->json('user_update')->nullable();
            $table->json('user_delete')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        // product detail
        Schema::create('product_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products');
            $table->integer('total_inv')->default(0);
            $table->integer('stocked_inv')->default(0);
            $table->integer('available_inv')->default(0);
            $table->decimal('unit_price', 15,2)->default(0.00);
            $table->decimal('bulk_price', 15,2)->default(0.00);
            $table->json('user_create')->nullable();
            $table->json('user_update')->nullable();
            $table->json('user_delete')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_detail');
    }
};
