<!DOCTYPE html>
<html>
<head>
    <title>Form Email</title>
</head>
<body>
    <p>Hi, {{ $mailData['name'] }}</p>
    <p>Our team would like to send you the purchase receipt!</p>
    <p>Thank you and best regards,</p>
    <p>Danhnc.</p>
</body>
</html>