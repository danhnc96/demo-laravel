@extends('layouts.app')

@section('content')
<div class="d-grid gap-2 d-md-block">
    <a class="btn btn-primary" href="{{ asset('contacts') }}">Contacts</a>
    <a class="btn btn-primary" href="{{ asset('product') }}">Products</a>
    <a class="btn btn-primary" href="{{ asset('order') }}">Order</a>
</div>
@endsection
