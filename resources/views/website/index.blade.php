@extends('website.layouts.app')

@section('content-website')
<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
    @foreach ($listProducts as $key => $value)
        <div class="col-md-3">
            <div class="card shadow-sm">
                @include('components.silde-image', ['data' => $value->image, 'id' => $key, 'height' => 220])
                <div class="card-body">
                    <div class="fs-5 fw-bold text-danger">{{ $value->ProductDetail->unit_price }} $</div>
                    <h6 class="card-title">{{ $value->name }}</h6>
                    <div class="card-text" style="height: 100px">{!! $value->summary !!}</div>
                    <div>
                        <a type="button" class="btn btn-warning float-end" href="{{ asset('/detail') }}/{{$value->id}}/{{$value->slug}}">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection