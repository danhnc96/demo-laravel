@extends('website.layouts.app')

@section('content-website')
<div class="row featurette">
    <div class="col-md-9 row">
        <div class="col-md-7 order-md-2">
            <h2 class="featurette-heading">{{ $product->name }}</span></h2>
            <input type="hidden" id="product_id" value="{{ $product->id }}">
            <hr class="featurette-divider">
            <div class="fs-5 fw-bold">
                Unit price: {{ $product->ProductDetail->unit_price }} $
                <input type="hidden" id="unit_price" value="{{ $product->ProductDetail->unit_price }}">
            </div>
            <div class="fs-5 fw-bold">
                Bulk price: {{ $product->ProductDetail->bulk_price }} $
                <input type="hidden" id="bulk_price" value="{{ $product->ProductDetail->bulk_price }}">
            </div>
            <hr class="featurette-divider">
            <div class="lead" style="min-height: 200px;">{!! $product->details !!}</div>
            <div class="col-md-12 mt-3 d-flex">
                <div class="col-md-5 p-2 d-flex">
                    <label>Select quantity</label>
                    <input type="number" class="form-control" name="quantity" id="quantity" value="1">
                </div>
                <div class="col-md-7 p-2 d-flex">
                    <label>Select Delivery Time & Date</label>
                    <select class="form-control" data-live-search="true" name="delivery" id="delivery">
                        @foreach (config('option.delivery') as $value)
                            <option value="{{$value['id']}}">{{$value['value']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <hr class="featurette-divider">
            <div class="col-md-12 p-2">
                <div id="total_money" class="fs-4 fw-bold text-danger float-start"><span>Total money: {{ $product->ProductDetail->unit_price }} $</span></div>
                <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#customer_information">
                    Continue
                </button>
            </div>
        </div>
        <div class="col-md-5 order-md-1">
            @include('components.silde-image', ['data' => $product->image, 'id' => 1, 'height' => 420])
        </div>
    </div>
    <div class="col-md-3 row">
        @if (!empty($related))
            <div class="col-md-12">
                <div class="card shadow-sm">
                    @include('components.silde-image', ['data' => $related->image, 'id' => 2, 'height' => 220])
                    <div class="card-body">
                        <h6 class="card-title">{{ $related->name }}</h6>
                        <div class="card-text" style="height: 100px">{!! $related->summary !!}</div>
                        <div>
                            <div class="fs-5 fw-bold text-danger float-start">{{ $related->ProductDetail->unit_price }} $</div>
                            <a type="button" class="btn btn-warning float-end" href="{{ asset('/detail') }}/{{$related->id}}/{{$related->slug}}">Buy Now</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="customer_information" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Customer Information</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="create-customer-form" novalidate method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    @include('components.alert-mess')
                    <div class="row g-3">
                        <div class="col-md-6">
                            <label for="title" class="form-label">Title</label>
                            <select class="form-select" id="title" name="title" required>
                                <option value="">select title</option>
                                @foreach (config('option.title') as $value)
                                    <option value="{{$value['id']}}">{{$value['value']}}</option>
                                @endforeach
                            </select>
                        </div>
                
                        <div class="col-sm-6">
                            <label for="first_name" class="form-label">First name</label>
                            <input type="text" class="form-control" id="first_name" name="first_name" value="" required>
                        </div>
                        <div class="col-sm-6">
                            <label for="last_name" class="form-label">Last name</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" value="" required>
                        </div>
                
                        <div class="col-sm-6">
                            <label for="second_name" class="form-label">Second name</label>
                            <input type="text" class="form-control" id="second_name" name="second_name" value="" required>
                        </div>
                        
                        <div class="col-12">
                            <label for="phone" class="form-label">Phone</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <select class="form-select" id="country_code" name="country_code" required>
                                        <option value="">select country code</option>
                                        @foreach (config('option.country_codes') as $value)
                                            <option value="{{$value['id']}}">{{$value['value']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" id="phone" name="phone" value="" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <label for="email" class="form-label">Email</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group has-validation">
                                        <span class="input-group-text">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                                                <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
                                            </svg>
                                        </span>
                                        <input type="email" class="form-control" id="email" name="email" value="" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                
                        <div class="col-12">
                            <label for="address" class="form-label">Address</label>
                            <input type="text" class="form-control" id="address" name="address" value="" required>
                        </div>
                
                        <div class="col-12">
                            <label for="suburb" class="form-label">Suburb</label>
                            <input type="text" class="form-control" id="suburb" name="suburb" value="" required>
                        </div>
                
                        <div class="col-sm-6">
                            <label for="state" class="form-label">State</label>
                            <input type="text" class="form-control" id="state" name="state" value="" required>
                        </div>
                
                        <div class="col-sm-6">
                            <label for="postcode" class="form-label">Postcode</label>
                            <input type="text" class="form-control" id="postcode" name="postcode" value="{{ !empty($data) ? $data->postcode : '' }}" required>
                        </div>
                
                        <div class="col-12">
                            <label for="country" class="form-label">Country</label>
                            <select class="form-select" id="country" name="country" value="{{ !empty($data) ? $data->country : '' }}" required>
                                <option value="">select country</option>
                                @foreach (config('option.country') as $value)
                                    <option value="{{$value['id']}}" {{ !empty($data) && $value['id'] == $data->country ? 'selected' : '' }}>{{$value['value']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="sumbit" class="btn btn-primary">Send Order</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- script --}}
<script>
// Total money 
$('#quantity').keyup(function() {
    let quantity = $(this).val();
    let unit_price = $('#unit_price').val();
    let bulk_price = $('#bulk_price').val();
    let total_money = unit_price;
    if(quantity < 50){
        total_money = unit_price * quantity;
    }else{
        total_money = bulk_price * quantity;
    }
    $('#total_money').children().remove();
    $('#total_money').append($('<span>').text('Total money: '+total_money));
});

// Create customer information
$('#create-customer-form').submit(function(e) {
    e.preventDefault();
    let formData = new FormData(this);
    formData.append('product_id', $('#product_id').val())
    formData.append('quantity', $('#quantity').val());
    formData.append('unit_price', $('#unit_price').val());
    formData.append('bulk_price', $('#bulk_price').val());
    formData.append('delivery', $('#delivery').val());
    $.ajax({
        url: "{{ route('customer.create') }}",
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: formData,
        contentType: false,
        processData: false,
        success: function(result) {
            console.log(result);
            alertSuccess('Create new contact successful!');
            window.location.replace("/demo-laravel/public/invoice/"+result['id']);
        },
        error: function(error) {
            alertError(error);
        }
    });
});
</script>
@endsection