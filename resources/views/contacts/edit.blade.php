@extends('layouts.app', ['title' => 'Contact | Edit'])

@section('content')
<div class="col-md-12">
    @include('components.alert-mess')
    @include('components.form-contacts', [$data])
</div>
@endsection
