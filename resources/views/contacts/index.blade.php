@extends('layouts.app', ['title' => 'Contact'])

@section('content')
@include('components.alert-mess')
<a href="{{ asset('contacts/create') }}" class="btn btn-primary float-end">
    Create contact
</a>
<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">#</th>
            @foreach (config('colum_table.contacts') as $value)
                <th scope="col">{{ $value['value'] }}</th>
            @endforeach
            <th class="col-md-1 text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($listContacts as $key => $value)
            <tr class="row_{{ $value->id }}">
                <th scope="row">{{ $key + 1 }}</th>
                <td>{{ config('option.title')['title_'.$value->title]['value'] }}</td>
                <td>{{ $value->first_name }}</td>
                <td>{{ $value->last_name }}</td>
                <td>{{ $value->email }}</td>
                <td>{{ $value->phone }}</td>
                <td>{{ $value->company }}</td>
                <td class="col-md-1 text-center">
                    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                        <a class="btn btn-primary btn-sm" href="{{ asset('contacts/edit') }}/{{$value->id}}">Edit</a>
                        <button class="btn btn-danger btn-sm" onclick="deleteContact({{ $value->id }})">Delete</button>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
{{ $listContacts->links()  }}

<script type="text/javascript">
    function deleteContact(deleteId){
        $.ajax({
            url: `contacts/delete/${deleteId}`,
            type: "delete",
            data: {
                id: deleteId,
                _token: '{!! csrf_token() !!}',
            },
            success: function(data) {
                console.log(data);
                $(`.row_${deleteId}`).remove();
                alertSuccess('Create new contact successful!');
            }
        });
    }
</script>
@endsection
