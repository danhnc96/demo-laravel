@extends('layouts.app', ['title' => 'Contact | Create'])

@section('content')
<div class="col-md-12">
    @include('components.alert-mess')
    @include('components.form-contacts')
</div>               
@endsection
