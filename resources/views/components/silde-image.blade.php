@php
    $list = json_decode($data);
@endphp
<div id="carouselExampleDark{{ $id }}" class="carousel carousel-dark slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
        @foreach($list as $key => $value)
            @if($key == 0)
            <button type="button" data-bs-target="#carouselExampleDark{{ $id }}" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            @else
            <button type="button" data-bs-target="#carouselExampleDark{{ $id }}" data-bs-slide-to="{{ $key }}" aria-label="Slide {{ $key + 1 }}"></button>
            @endif
        @endforeach
    </div>
    <div class="carousel-inner">
        @foreach($list as $key => $value)
            @if($key == 0)
            <div class="carousel-item active" data-bs-interval="2000">
            @else
            <div class="carousel-item" data-bs-interval="2000">
            @endif
                <img src="{{ asset('storage') }}/{{ $value }}" class="d-block w-100" alt="{{ $value }}" height="{{ $height }}">
            </div>
        @endforeach
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark{{ $id }}" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark{{ $id }}" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>