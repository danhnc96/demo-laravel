<!-- from contacts -->
<!-- @param data = aciton == edit ? array : null -->
<form id="create-contact-form" novalidate method="POST" enctype="multipart/form-data">
    @csrf
    @if (!empty($data))
        <input type="hidden" id="id" name="id" value="{{ $data->id }}">
    @endif
    <div class="row g-3">
        <div class="col-md-6">
            <label for="title" class="form-label">Title</label>
            <select class="form-select" id="title" name="title" required>
                <option value="">select title</option>
                @foreach (config('option.title') as $value)
                    <option value="{{$value['id']}}" {{ !empty($data) && $value['id'] == $data->title ? 'selected' : '' }}>{{$value['value']}}</option>
                @endforeach
            </select>
        </div>

        <div class="col-sm-6">
            <label for="first_name" class="form-label">First name</label>
            <input type="text" class="form-control" id="first_name" name="first_name" value="{{ !empty($data) ? $data->first_name : '' }}" required>
        </div>
        <div class="col-sm-6">
            <label for="last_name" class="form-label">Last name</label>
            <input type="text" class="form-control" id="last_name" name="last_name" value="{{ !empty($data) ? $data->last_name : '' }}" required>
        </div>

        <div class="col-sm-6">
            <label for="second_name" class="form-label">Second name</label>
            <input type="text" class="form-control" id="second_name" name="second_name" value="{{ !empty($data) ? $data->second_name : '' }}" required>
        </div>

        <div class="col-md-6">
            <label for="gender" class="form-label">Gender</label>
            <select class="form-select" id="gender" name="gender" required>
                <option value="">select gender</option>
                @foreach (config('option.gender') as $value)
                    <option value="{{$value['id']}}" {{ !empty($data) && $value['id'] == $data->gender ? 'selected' : '' }}>{{$value['value']}}</option>
                @endforeach
            </select>
        </div>

        <div class="col-sm-6">
            <label for="birthday" class="form-label">Date of birth</label>
            <input type="date" class="form-control" id="birthday" name="birthday" value="{{ !empty($data) ? $data->birthday : '' }}" required>
        </div>

        <div class="col-sm-6">
            <label for="photo" class="form-label">Photo</label>
            <input type="file" class="form-control load-image" id="photo" name="photo" required>
            @if(!empty($data))
                <img id="preview-image-before-upload" class="pt-2" src="{{ asset($data->photo) }}" alt="preview image" style="max-height: 100px;">
            @else
                <img id="preview-image-before-upload" class="d-none pt-2" src="" alt="preview image" style="max-height: 100px;">
            @endif
        </div>

        <div class="col-sm-6">
            <label for="company" class="form-label">Company</label>
            <div class="input-group has-validation">
                <span class="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-building" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M14.763.075A.5.5 0 0 1 15 .5v15a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5V14h-1v1.5a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5V10a.5.5 0 0 1 .342-.474L6 7.64V4.5a.5.5 0 0 1 .276-.447l8-4a.5.5 0 0 1 .487.022zM6 8.694 1 10.36V15h5V8.694zM7 15h2v-1.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 .5.5V15h2V1.309l-7 3.5V15z"/>
                        <path d="M2 11h1v1H2v-1zm2 0h1v1H4v-1zm-2 2h1v1H2v-1zm2 0h1v1H4v-1zm4-4h1v1H8V9zm2 0h1v1h-1V9zm-2 2h1v1H8v-1zm2 0h1v1h-1v-1zm2-2h1v1h-1V9zm0 2h1v1h-1v-1zM8 7h1v1H8V7zm2 0h1v1h-1V7zm2 0h1v1h-1V7zM8 5h1v1H8V5zm2 0h1v1h-1V5zm2 0h1v1h-1V5zm0-2h1v1h-1V3z"/>
                    </svg>
                </span>
                <input type="text" class="form-control" id="company" name="company" value="{{ !empty($data) ? $data->company : '' }}" required>
            </div>
        </div>

        <div class="col-12">
            <label for="phone" class="form-label">Phone</label>
            <div class="row">
                <div class="col-md-6">
                    <select class="form-select" id="country_code" name="country_code" required>
                        <option value="">select country code</option>
                        @foreach (config('option.country_codes') as $value)
                            <option value="{{$value['id']}}" {{ !empty($data) && $value['id'] == $data->country_code ? 'selected' : '' }}>{{$value['value']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <input type="number" class="form-control" id="phone" name="phone" value="{{ !empty($data) ? $data->phone : '' }}" required>
                </div>
            </div>
        </div>
        
        <div class="col-12">
            <label for="email" class="form-label">Email</label>
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group has-validation">
                        <span class="input-group-text">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                                <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
                            </svg>
                        </span>
                        <input type="email" class="form-control" id="email" name="email" value="{{ !empty($data) ? $data->email : '' }}" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <select class="form-select" id="email_type" name="email_type" value="{{ !empty($data) ? $data->email_type : '' }}" required>
                        <option value="">select email type</option>
                        @foreach (config('option.email_type') as $value)
                            <option value="{{$value['id']}}" {{ !empty($data) && $value['id'] == $data->email_type ? 'selected' : '' }}>{{$value['value']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="col-12">
            <label for="address" class="form-label">Address</label>
            <input type="text" class="form-control" id="address" name="address" value="{{ !empty($data) ? $data->address : '' }}" required>
        </div>

        <div class="col-12">
            <label for="suburb" class="form-label">Suburb</label>
            <input type="text" class="form-control" id="suburb" name="suburb" value="{{ !empty($data) ? $data->suburb : '' }}" required>
        </div>

        <div class="col-sm-6">
            <label for="state" class="form-label">State</label>
            <input type="text" class="form-control" id="state" name="state" value="{{ !empty($data) ? $data->state : '' }}" required>
        </div>

        <div class="col-sm-6">
            <label for="postcode" class="form-label">Postcode</label>
            <input type="text" class="form-control" id="postcode" name="postcode" value="{{ !empty($data) ? $data->postcode : '' }}" required>
        </div>

        <div class="col-12">
            <label for="country" class="form-label">Country</label>
            <select class="form-select" id="country" name="country" value="{{ !empty($data) ? $data->country : '' }}" required>
                <option value="">select country</option>
                @foreach (config('option.country') as $value)
                    <option value="{{$value['id']}}" {{ !empty($data) && $value['id'] == $data->country ? 'selected' : '' }}>{{$value['value']}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <hr class="my-4">
    <div class="col-md-12">
        <a href="{{ asset('contacts') }}" class="btn btn-secondary float-end mx-1">Close</a>
        <button type="sumbit" class="btn btn-info float-end mx-1">{{ !empty($data) ? 'Update' : 'Save' }}</button>
    <div>
</form>

<!-- ajax -->
<script type="text/javascript">
    $('#create-contact-form').submit(function(e) {
        let id = $('#id').val();
        var url = "{{ route('contacts.store') }}"
        if(id){
            url = "{{ route('contacts.update') }}"
        }
        e.preventDefault();
        let formData = new FormData(this);
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            contentType: false,
            processData: false,
            success: function(result) {
                console.log(result);
                alertSuccess('Create new contact successful!');
            },
            error: function(error) {
                alertError(error);
            }
        });
    });
</script>