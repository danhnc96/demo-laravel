<form id="create-product-form" method="post" enctype="multipart/form-data">
    @csrf
    @if (!empty($data))
        <input type="hidden" id="id" name="id" value="{{ $data->id }}">
    @endif
    <div class="form-group">
        <div class="col-md-12 d-flex">
            <div class="col-md-6 p-2">
                <label>Product Code</label>
                <input type="text" class="form-control" name="code" id="code" value="{{ !empty($data) ? $data->code : $code }}" readonly>
            </div>
            <div class="col-md-6 p-2">
                <label>Product Name</label>
                <input type="text" class="form-control" name="name" id="name" value="{{ !empty($data) ? $data->name : '' }}">
            </div>
        </div>
        <div class="col-md-12 d-flex">
            <div class="col-md-6 p-2">
                <label>Related Products</label>
                <select class="form-control selectpicker" data-live-search="true" name="related" id="related" value="{{ old('related') }}">
                    <option value="" hidden></option>
                    @foreach (config('option.category') as $value)
                        <option value="{{$value['id']}}" {{ !empty($data) && $value['id'] == $data->related ? 'selected' : '' }}>{{$value['value']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6 p-2">
                <label>Product Image</label>
                <input type="file" class="form-control" name="images[]" id="images" value="{{ !empty($data) ? $data->image : '' }}" accept="image/*" multiple>
            </div>
        </div>
        <div class="col-md-12 p-2">
            <label>Product Summary</label>
            <textarea class="form-control" name="summary" id="summary" cols="30" rows="10">{{ !empty($data) ? $data->summary : '' }}</textarea>
        </div>
        <div class="col-md-12 p-2">
            <label>Product Details</label>
            <textarea class="form-control" name="details" id="details" cols="30" rows="10">{{ !empty($data) ? $data->details : '' }}</textarea>
        </div>
    </div>
    <div class="form-group p-2 text-end">
        <button class="btn btn-primary" type="submit">{{ !empty($data) ? 'Update' : 'Save' }}</button>
        <a href="{{ asset('product') }}" class="btn btn-secondary">Close</a>
    </div>
</form>
<div class="modal fade" id="myModal" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Pricing and Inventory</h5>
        </div>
        <div id="modal-product" class="modal-body"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" onclick="location.reload()">Close</button>
          <button type="button" class="btn btn-primary" id="save-product-detail">Save</button>
        </div>
      </div>
    </div>
</div>

<script>
$(document).ready(function(e) {
    tinymce.init({
        selector: 'textarea#summary',
    });
    tinymce.init({
        selector: 'textarea#details',
    });
});

$('#create-product-form').submit(function(e) {
    let id = $('#id').val();
    var url = "{{ route('product.store') }}"
    if(id){
        url = "{{ route('product.update') }}"
    }
    e.preventDefault();
    let formData = new FormData(this);
    let totalImages = $('#images')[0].files.length;
    let images = $('#images')[0];
    var list_images = [];
    for (let i = 0; i < totalImages; i++) {
        list_images[i] = images.files[i]
    }
    formData.append('images', list_images);
    formData.append('summary', tinymce.get("summary").getContent());
    formData.append('details', tinymce.get("details").getContent());
    $.ajax({
        url: url,
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: formData,
        contentType: false,
        processData: false,
        success: function(result) {
            console.log(result)
            if(!id){
                showModalProductDetail(result['product_detail'])
            }
            alertSuccess('Create new product successful!');
        },
        error: function(error) {
            alertError(error);
        }
    });
});

$('#save-product-detail').click(function() {
    var product_id = $('#modal-product #product-detail #product_id').val();
    var id = $('#modal-product #product-detail #product_detail_id').val();
    var total_inv = $('#modal-product #product-detail #total_inv').val();
    var stocked_inv = $('#modal-product #product-detail #stocked_inv').val();
    var available_inv = $('#modal-product #product-detail #available_inv').val();
    var unit_price = $('#modal-product #product-detail #unit_price').val();
    var bulk_price = $('#modal-product #product-detail #bulk_price').val();
    updateProductDetail(id, product_id, total_inv, stocked_inv, available_inv, unit_price, bulk_price, 'updateDetail');
})

function showModalProductDetail(data){
    var code = $('#create-product-form #code').val();
    var name = $('#create-product-form #name').val();
    $('#modal-product').html(`
        <div id="product-detail">
            <input type="hidden" name="product_id" id="product_id" value="${data['product_id']}">
            <input type="hidden" name="product_detail_id" id="product_detail_id" value="${data['id']}">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Product Code</th>
                        <th class="col">Product Name</th>
                        <th scope="col">Total Inventory</th>
                        <th class="col">Stocked Inventory</th>
                        <th scope="col">Available Inventory</th>
                        <th class="col">Unit Price</th>
                        <th class="col">Bulk Price</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope="row">${code}</td>
                        <td scope="row">${name}</td>
                        <td scope="row"><input type="text" class="form-control" name="total_inv" id="total_inv" value="0"></td>
                        <td scope="row"><input type="text" class="form-control" name="stocked_inv" id="stocked_inv" value="0"></td>
                        <td scope="row"><input type="text" class="form-control" name="available_inv" id="available_inv" value="0"></td>
                        <td scope="row"><input type="text" class="form-control" name="unit_price" id="unit_price" value="0.0"></td>
                        <td scope="row"><input type="text" class="form-control" name="bulk_price" id="bulk_price" value="0.0"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    `)
    $('#myModal').modal('show');
    $('#myModal').modal({backdrop: "static"});
}
</script>