<!-- Success Alert -->
<div class="alert alert-success alert-dismissible fade show d-none" id="success">
    <div id="succText"></div>
</div>
<!-- Error Alert -->
<div class="alert alert-danger alert-dismissible fade show d-none" id="error">
    <div id="errText"></div>
</div>