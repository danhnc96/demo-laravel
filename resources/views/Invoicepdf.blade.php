<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Invoice</title>
  </head>
  <body style="margin: -30px;">
    <table style="width:100%">
      <tr>
        <td colspan="3"><h1 style="color: #0e26af; font-size: 50px; padding: 0.2rem 0;">Invoice</h1></td>
      </tr>
      <tr>
        <td><h3>Invoice for</h3></td>
        <td><h3>Payable to</h3></td>
        <td><h3>Invoice #</h3></td>
      </tr>
      <tr>
        <td><p>Name: {{ $data->CustomerInformation->first_name	}} {{ $data->CustomerInformation->last_name	}}</p></td>
        <td rowspan="2"><p>Development Company</p></td>
        <td rowspan="2"><p>INV-{{$data->id}}</p></td>
      </tr>
      <tr>
        <td><p>Address: {{ $data->CustomerInformation->address	}}</p></td>
      </tr>
      <tr>
        <td></td>
        <td><h3>Order Date</h3></td>
        <td><h3>Account</h3></td>
      </tr>
      <tr>
        <td></td>
        <td><p>{{ $data->created_at	}}</p></td>
        <td><p>{{ $data->id	}}</p></td>
      </tr>
    </table>
    <hr />
    <table style="width:100%">
      <tr>
        <td><h3 style="color: #0e26af;">Product Name</h3></td>
        <td><h3 style="color: #0e26af;">Product Code</h3></td>
        <td><h3 style="color: #0e26af;">Quantiny</h3></td>
        <td><h3 style="float: right; color: #0e26af;">Price</h3></td>
        <td><h3 style="float: right; color: #0e26af;">Total Price</h3></td>
      </tr>
      <tr style="background: #e1dede;">
        <td><p>{{ $data->Product->name	}}</p></td>
        <td><p>{{ $data->Product->code	}}</p></td>
        <td><p>{{ $data->quantity }}</p></td>
        <td><p style="float: right;">${{ $data->unit_price }}</p></td>
        <td><p style="float: right;">${{ $data->total_money }}</p></td>
      </tr>
      <tr>
        <td><br></td>
        <td><br></td>
        <td><br></td>
        <td><p style="float: right; color: #0e26af;">Subtotal</p></td>
        <td><p style="float: right; font-weight: bold;">${{ $data->total_money }}</p></td>
      </tr>
      <tr>
        <td><br></td>
        <td><br></td>
        <td><br></td>
        <td><p style="float: right; color: #0e26af;">GST</p></td>
        <td><p style="float: right;">${{ $data->total_money*0.1 }}</p></td>
      </tr>
      <tr>
        <td><br></td>
        <td><br></td>
        <td><br></td>
        <td><h2 style="float: right; color: #e91e63;">AUD</h2></td>
        <td><h2 style="float: right; color: #e91e63;">${{ $data->total_money + $data->total_money*0.1 }}</h2></td>
      </tr>
    </table>
    <br /><br />
    <table style="width:100%; border-spacing: 0;">
      <tr style="border-color: inherit;">
        <td style="background: #383737; padding: 10px;"><h3 style="color: #fff;">Payment Method</h3></td>
      </tr>
      <tr style="background-color: #dddddd;">
        <td style="padding: 10px;"><p><strong>Bank Transfer</strong></p></td>
      </tr>
      <tr style="background-color: #dddddd;">
        <td style="padding: 10px;"><p><strong>0985 3473 8333 1234</strong></p></td>
      </tr>
      <tr style="background-color: #dddddd;">
        <td style="padding: 10px;"><p><strong>Credit Card:</strong> Complete the credit card form. Payment by credit card may incur surcharge up to 3 % per transaction.</p></td>
      </tr>
      <tr style="background-color: #dddddd;">
        <td style="padding: 10px;"><p><span style="margin-right: 35px">Name on Card: ___________________________</span><span>Card Number: ___________________________</span></p></td>
      </tr>
      <tr style="background-color: #dddddd;">
        <td style="padding: 10px;"><p><span style="margin-right: 35px">Signature: ___________________________</span><span style="margin-right: 35px">Expiry date: ____________</span><span>CVV: ____________</span></p></td>
      </tr>
    </table>
    <style>
      p, h1, h2, h3 {
        margin: 2px;
      }
    </style>
  </body>
</html>