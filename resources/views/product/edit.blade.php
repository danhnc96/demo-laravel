@extends('layouts.app', ['title' => 'Product | Edit'])

@section('content')
<div class="col-md-12">
    @include('components.alert-mess')
    @include('components.form-product', [$data])
</div>               
@endsection
