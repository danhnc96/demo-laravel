@extends('layouts.app', ['title' => 'Product'])

@section('content')
@include('components.alert-mess')
<a href="{{ asset('product/create') }}" class="btn btn-primary float-end">
    Create product
</a>
<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">#</th>
            @foreach (config('colum_table.product') as $value)
                <th scope="col">{{ $value['value'] }}</th>
            @endforeach
            <th class="col-md-1 text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($listProducts as $key => $value)
        <tr>
            <th scope="row">{{ $key + 1 }}</th>
            <td scope="row">{{ $value->code }}</td>
            <td scope="row">{{ $value->name }}</td>
            <td scope="row">{{ $value->ProductDetail->total_inv }}</td>
            <td scope="row">{{ $value->ProductDetail->stocked_inv }}</td>
            <td scope="row">{{ $value->ProductDetail->available_inv }}</td>
            <td scope="row">{{ $value->ProductDetail->unit_price }}</td>
            <td scope="row">{{ $value->ProductDetail->bulk_price }}</td>
            <td class="col-md-1 text-center">
                <div class="dropdown">
                    <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        Action product
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                      <li><a class="dropdown-item" href="{{ asset('product/edit') }}/{{$value->id}}">Edit product</a></li>
                      <li><button class="dropdown-item" onclick="editProductDetail('{{ $value->code }}', '{{ $value->name }}', {{ $value->ProductDetail->id }}, {{ $value->ProductDetail->product_id }}, '{{ $value->ProductDetail->total_inv }}', '{{ $value->ProductDetail->stocked_inv }}', '{{ $value->ProductDetail->available_inv }}','{{ $value->ProductDetail->unit_price }}', '{{ $value->ProductDetail->bulk_price }}')">Edit pricing product</button></li>
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $listProducts->links()  }}

<div class="modal fade" id="myModalDetail" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Pricing and Inventory</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div id="modal-product-detail" class="modal-body"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="update-product-detail">Update</button>
        </div>
      </div>
    </div>
</div>

<script>
function editProductDetail(code, name, id, product_id, total_inv, stocked_inv, available_inv, unit_price, bulk_price){
    $('#modal-product-detail').html(`
        <div id="product-detail">
            <input type="hidden" name="product_id" id="product_id" value="${product_id}">
            <input type="hidden" name="product_detail_id" id="product_detail_id" value="${id}">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Product Code</th>
                        <th class="col">Product Name</th>
                        <th scope="col">Total Inventory</th>
                        <th class="col">Stocked Inventory</th>
                        <th scope="col">Available Inventory</th>
                        <th class="col">Unit Price</th>
                        <th class="col">Bulk Price</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope="row">${code}</td>
                        <td scope="row">${name}</td>
                        <td scope="row"><input type="text" class="form-control" name="total_inv" id="total_inv" value="${total_inv}"></td>
                        <td scope="row"><input type="text" class="form-control" name="stocked_inv" id="stocked_inv" value="${stocked_inv}"></td>
                        <td scope="row"><input type="text" class="form-control" name="available_inv" id="available_inv" value="${available_inv}"></td>
                        <td scope="row"><input type="text" class="form-control" name="unit_price" id="unit_price" value="${unit_price}"></td>
                        <td scope="row"><input type="text" class="form-control" name="bulk_price" id="bulk_price" value="${bulk_price}"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    `)
    $('#myModalDetail').modal('show');
}

$('#update-product-detail').click(function() {
    var product_id = $('#modal-product-detail #product-detail #product_id').val();
    var id = $('#modal-product-detail #product-detail #product_detail_id').val();
    var total_inv = $('#modal-product-detail #product-detail #total_inv').val();
    var stocked_inv = $('#modal-product-detail #product-detail #stocked_inv').val();
    var available_inv = $('#modal-product-detail #product-detail #available_inv').val();
    var unit_price = $('#modal-product-detail #product-detail #unit_price').val();
    var bulk_price = $('#modal-product-detail #product-detail #bulk_price').val();
    updateProductDetail(id, product_id, total_inv, stocked_inv, available_inv, unit_price, bulk_price, 'product/updateDetail');
})
</script>
@endsection
