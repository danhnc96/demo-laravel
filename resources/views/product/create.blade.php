@extends('layouts.app', ['title' => 'Product | Create'])

@section('content')
<div class="col-md-12">
    @include('components.alert-mess')
    @include('components.form-product', [$code])
</div>               
@endsection
