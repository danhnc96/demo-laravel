@extends('layouts.app', ['title' => 'Order | Detail'])

@section('content')
@include('components.alert-mess')
<div class="form-group">
    <div class="row">
        <div class="col-md-7" style="border-right: 1px solid;">
            <div class="row">
                <div class="col-md-6 p-2">
                    <label>Order Date</label>
                    <input type="datetime" class="form-control" name="created_at" id="created_at" value="{{ $data->created_at }}" disabled>
                </div>
                <div class="col-md-6 p-2">
                    <label>Order Number</label>
                    <input type="text" class="form-control" name="id" id="id_order" value="{{ $data->id }}" disabled>
                </div>
                <div class="col-md-6 p-2">
                    <label>Product Quantity</label>
                    <input type="number" class="form-control" name="quantity" id="quantity" value="{{ $data->quantity }}" disabled>
                </div>
                <div class="col-md-6 p-2">
                    <label>Price Detail</label>
                    <input type="text" class="form-control" name="total_money" id="total_money" value="{{ $data->total_money }}" disabled>
                </div>
                <div id="view-payment-status"class="col-md-6 p-2"></div>
                <div class="col-md-12 p-2">
                    <label>Customer Detail</label>
                    <div id="form-customer" class="form-customer">
                        <p class="m-0">- Name: {{ $data->CustomerInformation->first_name }} {{ $data->CustomerInformation->last_name }}</p>
                        <p class="m-0">- Phone: ( +{{ $data->CustomerInformation->country_code }}) {{ $data->CustomerInformation->phone }}</p>
                        <p class="m-0">- Email: {{ $data->CustomerInformation->email }}</p>
                        <p class="m-0">- Address: {{ $data->CustomerInformation->address }}</p>
                        <p class="m-0">- Suburb: {{ $data->CustomerInformation->suburb }}</p>
                        <p class="m-0">- State: {{ $data->CustomerInformation->state }}</p>
                        <p class="m-0">- Postcode: {{ $data->CustomerInformation->postcode }}</p>
                        <p class="m-0">- Country: {{ $data->CustomerInformation->country }} 
                            <button type="button" class="btn btn-outline-primary btn-sm float-end" data-bs-toggle="modal" data-bs-target="#customer_information">
                                Edit
                            </button>
                        </p>
                    </div>
                    <button id="send-mail" class="btn btn-primary float-end mt-3" type="button" onclick="sendMailInvoice('{{ $data->CustomerInformation->email }}', '{{ $data->CustomerInformation->first_name }}', '{{ $data->id }}')">Send Invoice To Customer</button>
                </div>
            </div>
        </div>
        <div id="form_order_warehouse" class="col-md-5">
            <div class="row">
                <div class="col-md-12 p-2">
                    <label>Product Name</label>
                    <input type="text" class="form-control" name="created_at" id="created_at" value="{{ $data->Product->name }}" disabled>
                </div>
                <div class="col-md-12 p-2">
                    <label>Product Code</label>
                    <input type="text" class="form-control" name="created_at" id="created_at" value="{{ $data->Product->code }}" disabled>
                </div>
                <div class="col-md-12 p-2">
                    <label>Delivery Time & Date</label>
                    <input type="datetime" class="form-control" name="delivery_at" id="delivery_at" value="{{ $data->delivery_at }}">
                </div>
                <div class="col-md-12 p-2">
                    <label>Delivery address</label>
                    <input type="datetime" class="form-control" name="delivery_address" id="delivery_address" value="{{ $data->delivery_address }}">
                </div>
                <div id="view-delivery-progress" class="col-md-12 p-2"></div>
                <div class="col-md-12 p-2">
                    <button class="btn btn-primary float-end" type="button" onclick="updateOrder()">Send Order To Warehouse</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="customer_information" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Customer Information</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="update-customer-form" novalidate method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="customer_id" name="customer_id" value="{{ $data->CustomerInformation->id }}">
                <div class="modal-body">
                    <div class="row g-3">
                        <div class="col-sm-6">
                            <label for="first_name" class="form-label">First name</label>
                            <input type="text" class="form-control" id="first_name" name="first_name" value="{{ $data->CustomerInformation->first_name }}" required>
                        </div>
                        <div class="col-sm-6">
                            <label for="last_name" class="form-label">Last name</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $data->CustomerInformation->last_name }}" required>
                        </div>
                        <div class="col-12">
                            <label for="phone" class="form-label">Phone</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <select class="form-select" id="country_code" name="country_code" required>
                                        <option value="">select country code</option>
                                        @foreach (config('option.country_codes') as $value)
                                            <option value="{{$value['id']}}" {{ $value['id'] == $data->CustomerInformation->country_code ? 'selected' : '' }}>{{$value['value']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" id="phone" name="phone" value="{{ $data->CustomerInformation->phone }}" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <label for="email" class="form-label">Email</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group has-validation">
                                        <span class="input-group-text">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                                                <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
                                            </svg>
                                        </span>
                                        <input type="email" class="form-control" id="email" name="email" value="{{ $data->CustomerInformation->email }}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                
                        <div class="col-12">
                            <label for="address" class="form-label">Address</label>
                            <input type="text" class="form-control" id="address" name="address" value="{{ $data->CustomerInformation->address }}" required>
                        </div>
                
                        <div class="col-12">
                            <label for="suburb" class="form-label">Suburb</label>
                            <input type="text" class="form-control" id="suburb" name="suburb" value="{{ $data->CustomerInformation->suburb }}" required>
                        </div>
                
                        <div class="col-sm-6">
                            <label for="state" class="form-label">State</label>
                            <input type="text" class="form-control" id="state" name="state" value="{{ $data->CustomerInformation->state }}" required>
                        </div>
                
                        <div class="col-sm-6">
                            <label for="postcode" class="form-label">Postcode</label>
                            <input type="text" class="form-control" id="postcode" name="postcode" value="{{ $data->CustomerInformation->postcode }}" required>
                        </div>
                
                        <div class="col-12">
                            <label for="country" class="form-label">Country</label>
                            <select class="form-select" id="country" name="country" required>
                                <option value="">select country</option>
                                @foreach (config('option.country') as $value)
                                    <option value="{{$value['id']}}" {{ $value['id'] == $data->CustomerInformation->country ? 'selected' : '' }}>{{$value['value']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="sumbit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<style>
    .form-customer{
        padding: 10px;
        border-radius: 5px;
        border: 1px solid #ced4da;
        background: #e9ecef;
    }
</style>
<script>
    $('#update-customer-form').submit(function(e) {
        e.preventDefault();
        let formData = new FormData(this);
        $.ajax({
            url: "{{ route('customer.update') }}",
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            contentType: false,
            processData: false,
            success: function(result) {
                alert('Update customer successful!');
                location.reload();
            },
            error: function(error) {
                alert(error);
            }
        });
    });

    function sendMailInvoice(email, name, id) {
        $('#send-mail').attr('disabled','disabled');
        $.ajax({
            url: "{{ route('sendMailInvoice') }}",
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data:  {
                email: email,
                id: id,
                name: name,
            },
            success: function(result) {
                alertSuccess('Send Mail Successfully!');
                $('#send-mail').removeAttr('disabled');
            },
            error: function(error) {
                alertError(error);
                $('#send-mail').removeAttr('disabled');
            }
        });
    }

    function updateOrder() {
        $.ajax({
            url: "{{ route('order.update') }}",
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data:  {
                delivery_at: $('#form_order_warehouse #delivery_at').val(),
                delivery_address: $('#form_order_warehouse #delivery_address').val(),
                id: $('#id_order').val(),
            },
            success: function(result) {
                $('#view-payment-status').html(`
                    <label>Payment Status</label>
                    <input type="text" class="form-control" name="status" id="status" value="{{ config('option.status_order')[$data->status]['value'] }}" disabled>
                `);
                $('#view-delivery-progress').html(`
                    <label>Delivery Progress</label>
                    <input type="text" class="form-control" name="delivery_progress" id="delivery_progress" value="wait for confirmation" disabled>
                `);
            },
            error: function(error) {
                alertError(error);
            }
        });
    }
</script>
@endsection
