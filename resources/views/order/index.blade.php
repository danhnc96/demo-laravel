@extends('layouts.app', ['title' => 'Order'])

@section('content')
@include('components.alert-mess')
<table class="table table-striped">
    <thead>
        <tr>
            @foreach (config('colum_table.order') as $value)
                <th scope="col">{{ $value['value'] }}</th>
            @endforeach
            <th class="col-md-1 text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($listOrder as $key => $value)
        <tr>
            <td scope="row">{{ $value->id }}</td>
            <td scope="row">{{ $value->Product->code }}</td>
            <td scope="row">{{ $value->total_money }}</td>
            <td scope="row">{{ config('option.status_order')[$value->status]['value'] }}</td>
            <td scope="row">{{ $value->created_at }}</td>
            <td class="col-md-1 text-center">
                <a class="btn btn-primary" href="{{ asset('order/detail') }}/{{$value->id}}">View</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $listOrder->links()  }}
@endsection
