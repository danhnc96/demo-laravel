<?php

return [
    // Title options
    'title' => [
        'title_1' => ['id' => 1, 'value' => 'title 1'],
        'title_2' => ['id' => 2, 'value' => 'title 2'],
        'title_3' => ['id' => 3, 'value' => 'title 3'],
        'title_4' => ['id' => 4, 'value' => 'title 4'],
        'title_5' => ['id' => 5, 'value' => 'title 5'],
    ],

    // Gender options
    'gender' => [
        'male' => ['id' => 'male', 'value' => 'Male'],
        'female' => ['id' => 'female', 'value' => 'Female'],
        'other' => ['id' => 'other', 'value' => 'Other'],
    ],

    // List of country calling codes
    'country_codes' => [
        'australia' => ['id' => 61, 'value' => '+61 (Australia)'],
        'australian_atarctic_territory' => ['id' => 6721, 'value' => '+672 1 (Australian Antarctic Territory)'],
        'australian_external_territories' => ['id' => 672, 'value' => '+672 (Australian External Territories)'],
        'vietnam' => ['id' => 84, 'value' => '+84 (Vietnam)'],
        'united_states' => ['id' => 1, 'value' => '+1 (United States)'],
    ],

    // List country
    'country' => [
        'australia' => ['id' => 'australia', 'value' => 'Australia'],
        'vietnam' => ['id' => 'vietnam', 'value' => 'Viet Nam'],
        'united_states' => ['id' => 'united_states', 'value' => 'United States'],
    ],

    // List email type
    'email_type' => [
        'gmail' => ['id' => 'gmail', 'value' => 'Gmail'],
        'aol' => ['id' => 'aol', 'value' => 'AOL Mail'],
        'outlook' => ['id' => 'outlook', 'value' => 'Outlook'],
        'yahoo' => ['id' => 'yahoo', 'value' => 'Yahoo! Mail'],
        'icloud' => ['id' => 'icloud', 'value' => 'iCloud Mail'],
        'gmx' => ['id' => 'gmx', 'value' => 'GMX Mail'],
    ],

    // Category product
    'category' => [
        '1' => ['id' => 1, 'value' => 'product 1'],
        '2' => ['id' => 2, 'value' => 'product 2'],
        '3' => ['id' => 3, 'value' => 'product 3'],
        '4' => ['id' => 4, 'value' => 'product 4'],
        '5' => ['id' => 5, 'value' => 'product 5'],
        '6' => ['id' => 6, 'value' => 'product 6'],
        '7' => ['id' => 7, 'value' => 'product 7'],
        '8' => ['id' => 8, 'value' => 'product 8'],
        '9' => ['id' => 9, 'value' => 'product 9'],
        '10' => ['id' => 10, 'value' => 'product 10'],
    ],

    // form of delivery
    'delivery' => [
        '1' => ['id' => 1, 'value' => 'Office Hour Delivery'],
        '2' => ['id' => 2, 'value' => 'Delivery Any Hour'],
        '3' => ['id' => 3, 'value' => 'Weekend Delivery'],
    ],

    // status order
    'status_order' => [
        '0' => ['id' => 0, 'value' => 'Order Requested'],
        '1' => ['id' => 1, 'value' => 'Confirmed Unpaid'],
        '2' => ['id' => 2, 'value' => 'Confirmed Paid'],
    ],
];
