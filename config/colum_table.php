<?php

return [
    // Table colum list contacts
    'contacts' => [
        'title' => ['id' => 1, 'value' => 'Title'],
        'first_name' => ['id' => 2, 'value' => 'First name'],
        'last_name' => ['id' => 3, 'value' => 'Last name'],
        'email' => ['id' => 4, 'value' => 'Email'],
        'phone' => ['id' => 5, 'value' => 'Phone'],
        'copmpany' => ['id' => 6, 'value' => 'Company'],
    ],

    // Table colum list product
    'product' => [
        'product_code' => ['id' => 1, 'value' => 'Product Code'],
        'product_name' => ['id' => 2, 'value' => 'Product Name'],
        'total_inventory' => ['id' => 3, 'value' => 'Total Inventory'],
        'stocked_inventory' => ['id' => 4, 'value' => 'Stocked Inventory'],
        'available_inventory' => ['id' => 5, 'value' => 'Available Inventory'],
        'unit_price' => ['id' => 6, 'value' => 'Unit Price'],
        'bulk_price' => ['id' => 6, 'value' => 'Bulk Price'],
    ],

    // Table colum list order
    'order' => [
        'order_number' => ['id' => 1, 'value' => 'Order Number'],
        'product_code' => ['id' => 2, 'value' => 'Product Code'],
        'price' => ['id' => 3, 'value' => 'Price'],
        'status' => ['id' => 4, 'value' => 'Status'],
        'order_date_time' => ['id' => 5, 'value' => 'Order Date & Time'],
    ],
];
