<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route website
Route::get('/', [App\Http\Controllers\WebsiteController::class, 'index'])->name('index');
Route::get('detail/{id}/{slug}', [App\Http\Controllers\WebsiteController::class, 'detail'])->name('detail');
Route::prefix('/customer')->name('customer.')->group(function(){
    Route::post('/create', [App\Http\Controllers\WebsiteController::class, 'createCustomerInformation'])->name('create');
    Route::post('/update', [App\Http\Controllers\WebsiteController::class, 'updateCustomerInformation'])->name('update');
});
Route::get('invoice/{id}', [App\Http\Controllers\WebsiteController::class, 'invoice'])->name('invoice');
// download PDF
Route::get('/downloadPDF/{id}',[App\Http\Controllers\DownloadController::class, 'downloadPDF'])->name('downloadPDF');

Auth::routes();
Route::group(['middleware' => ['auth' ]], function () {
    // Route dashboard
    Route::prefix('/dashboard')->name('dashboard.')->group(function(){
        Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('index');
    });

    // Route contacts
    Route::prefix('/contacts')->name('contacts.')->group(function(){
        Route::get('/', [App\Http\Controllers\ContactController::class, 'index'])->name('index');
        Route::get('/create', [App\Http\Controllers\ContactController::class, 'create'])->name('create');
        Route::post('/store', [App\Http\Controllers\ContactController::class, 'store'])->name('store');
        Route::get('/edit/{id}', [App\Http\Controllers\ContactController::class, 'show'])->name('show');
        Route::post('/update', [App\Http\Controllers\ContactController::class, 'update'])->name('update');
        Route::delete('delete/{id}', [App\Http\Controllers\ContactController::class, 'delete'])->name('delete');
    });

    // Route product
    Route::prefix('/product')->name('product.')->group(function(){
        Route::get('/', [App\Http\Controllers\ProductController::class, 'index'])->name('index');
        Route::get('/create', [App\Http\Controllers\ProductController::class, 'create'])->name('create');
        Route::post('/store', [App\Http\Controllers\ProductController::class, 'store'])->name('store');
        Route::get('/edit/{id}', [App\Http\Controllers\ProductController::class, 'show'])->name('show');
        Route::post('/update', [App\Http\Controllers\ProductController::class, 'update'])->name('update');
        Route::delete('delete/{id}', [App\Http\Controllers\ProductController::class, 'delete'])->name('delete');
        Route::post('/updateDetail', [App\Http\Controllers\ProductController::class, 'updateDetail'])->name('updateDetail');
    });

    // Route order
    Route::prefix('/order')->name('order.')->group(function(){
        Route::get('/', [App\Http\Controllers\OrderController::class, 'index'])->name('index');
        Route::get('detail/{id}', [App\Http\Controllers\OrderController::class, 'detail'])->name('detail');
        Route::post('/update', [App\Http\Controllers\OrderController::class, 'update'])->name('update');
    });

    Route::post('send-mail', [App\Http\Controllers\MailController::class, 'sendMailInvoice'])->name('sendMailInvoice');
});